Yaksh Plagiarism App
====================


A Yaksh specific django app to detect and flag copied answers for a quiz. 
This is written and (hopfully!) maintained as a part of FOSSEE fellowship 
by the following people - 

- Interns
  
  - Ayan Banerjee
  - Tanmay Nandanwar

- Mentor
  
  - Mahesh Gudi
