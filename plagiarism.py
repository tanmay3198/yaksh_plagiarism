import os

import pycode_similar


class Plagiarism(object):

    def __init__(self, length, percent):

        self.threshold_length = length
        self.threshold_percent = percent


    def read_files(self):

        path = os.getcwd()
        self.files = os.listdir(path)
        current_file = os.path.basename(__file__)
        self.files.remove(current_file)

        return self.files


    def calculate_percent(self, files):

        # True if code is found to be plagiarised otherwise False
        self.plag_flag = [False]*len(files)

        n = len(files)

        i = 0
        while i < n:
            # loop to select ref file such that it is not found
            # to be plagiarised in any of the previous iterations
            while self.plag_flag[i] == True:
                i += 1
                if i == n:
                    break

            if i >= n:
                break

            ref_file = open(files[i]).read()

            if len(ref_file) < self.threshold_length:
                i += 1
                continue
            else:
                j = i+1
                temp_array = []
                temp_array = [
                    [
                        file for file in self.files[j:]
                        if self.plag_flag[j] == False and
                        len(open(file).read()) >= self.threshold_length
                        ],
                    [
                        open(file).read() for file in self.files[j:]
                        if self.plag_flag[j] == False and
                        len(open(file).read()) >= self.threshold_length
                        ]
                ]

                temp_array[0].insert(0, files[i])
                temp_array[1].insert(0, ref_file)
                plag = pycode_similar.detect(temp_array[1])

                plag_percent_array = [
                    [
                        files[i+1] for i in range(len(plag))
                        if plag[0][1][i].plagiarism_percent*100 >=
                        self.threshold_percent
                        ],
                    [
                        plag[0][1][i].plagiarism_percent*100
                        for i in range(len(plag))
                        if plag[0][1][i].plagiarism_percent*100 >=
                        self.threshold_percent
                        ]
                ]

                if len(plag_percent_array[1]) > 0:
                    plag_percent_array[0].insert(0, self.files[i])

                for file in plag_percent_array[0]:
                    ind = self.files.index(file)
                    self.plag_flag[ind] = True
            i += 1
